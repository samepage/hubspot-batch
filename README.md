# HubSpot Batch Import/Export #

![picture](https://bitbucket.org/repo/GX79K9j/images/2570800071-01.png)

## 使用方法

プログラムの実行方法は2種類あります。

* Webインターフェースで実行する。
* スクリプトで実行する。

この2つの方法は、実は同じです。  
しかし、Webインターフェースは、必要なパラメータの生成を支援するためのものです。  
このプログラムは、手動でも、SchedulerやCRONによる自動化でも使用できます。

### Webインターフェースで実行する。
プログラムをインストールしたら、ウェブサイトのルートディレクトリを開きます（例えば、http://127.0.0.1）。  
以下のスクリーンショットのように表示されます。  
![picture](https://bitbucket.org/repo/GX79K9j/images/2570800071-01.png)

### スクリプトで実行する。
プログラムのURLに直接アクセスできます。  
これは、CRON JOBやSchedulerで特定の時間帯にプログラムを実行するように設定する場合に便利です。

以下の構造とパラメーターでプログラムにアクセスできます。  
`[root_directory_program]/?command=export&export_start_at=2021-06-09+00%3A00%3A00&export_end_at=2021-06-30&data_type=deal`

例えば、こんな感じです。  
`http://127.0.0.1/?command=export&export_start_at=2021-06-09+00%3A00%3A00&export_end_at=2021-06-30&data_type=deal`


## エクスポート機能の使い方

### ウェブインターフェースを使ってデータをエクスポートする。
①エクスポートしたいデータの日付の範囲を入力する。  

②次に、エクスポート機能を選択します。(コンタクトや取引)。

![picture](https://bitbucket.org/repo/GX79K9j/images/2361286155-02.png)

③プログラムは、エクスポートしたデータの結果を画面に表示します。  
![picture](https://bitbucket.org/repo/GX79K9j/images/180951160-Screenshot%202021-06-14%20163649.png)

また、CSVファイルは設定ファイルで指定した場所に保存されます。
![picture](https://bitbucket.org/repo/GX79K9j/images/2071002048-Screenshot%202021-06-14%20163801.png)

### スケジューラーやCRONを使ったデータのエクスポート
気をつけなければならないのは4つのパラメータです。

* **「command」**：「command」パラメータは 「export」に設定する必要があります。
* **「export_start_at」**：エクスポートしたいデータセットの「start_date」を設定する。
* **「export_end_at」**：エクスポートしたいデータセットの「end_date」を設定する。
* **「data_type」**：「data_type」は、「contact」または「deal」のいずれかを設定できます。

## インポート機能の使い方
### ウェブインターフェースを使ってデータをインポートする。
①CSVファイル名と、CSVファイルのデータの開始行を入力してください。

②次に、インポート機能を選択します。(コンタクトや取引や製品)。

### スケジューラーやCRONを使ったデータのインポート
気をつけなければならないのは4つのパラメータです。

* **「command」**：「command」パラメータは 「export」に設定する必要があります。
* **「import_filename」**：ファイル名。
* **「import_start_row」**：データのスタートラインです。
* **「data_type」**：「data_type」は、「contact」、「deal」または「product」のいずれかを設定できます。


## 構成の説明。
「config.php」ファイルには、プログラムのすべての設定が表示されています。  
以下の設定変数を変更することができます。
![picture](https://bitbucket.org/repo/GX79K9j/images/1122645267-Screenshot%202021-06-14%20165206.png)

#### $debug

結果のデータを画面に表示したい場合は、
この変数を "true "に設定します。"false "にすると、情報を隠します。

#### $dry_test

実際のCSVファイルを作成せず、プログラムのテストのみを行いたい場合は、
この変数を「true」に設定してください（プログラムはファイルにデータを書き込みません）。

#### $hubspot_api_key

HubSpotのAPIキー（このキーは、HubSpotのアカウント設定ページから取得できます）。

#### EXPORT_LOCATION

ファイルをエクスポートする先のディレクトリです。

#### $data_contact_fields

CSVファイルにエクスポートしたいHubSpotのコンタクトのプロパティのリストです。
CSVのエクスポートフィールドを変更したい場合は、この変数を更新してください。

#### $data_deal_fields

CSVファイルにエクスポートしたいHubSpotの取引のプロパティのリストです。
CSVのエクスポートフィールドを変更したい場合は、この変数を更新してください。

#### $data_line_item_fields

CSVファイルにエクスポートしたいHubSpotの製品のプロパティのリストです。
CSVのエクスポートフィールドを変更したい場合は、この変数を更新してください。
