<?php

class App
{
    private static $instances;

    protected $api_client;
    protected $command;
    protected $command_params;
    protected $data_type;
    protected $params;

    protected function __construct() { }
    protected function __clone() { }

    public static function getInstance()
    {
        if (!self::$instances) {
            self::$instances = new static();
        }

        return self::$instances;
    }

    /**
     * @param \HubSpotClient $api_client  API Client class.
     * @param string $command             Either「export」or「import」.
     * @param string $data_type           Either「contact」or「deal」.
     * @param array $command_params       Parameters that are being used for that specific command.
     */
    public function init($api_client, $command, $data_type, $command_params)
    {
        $this->api_client     = $api_client;
        $this->command        = $command;
        $this->data_type      = $data_type;
        $this->command_params = $command_params;

        return self::$instances;
    }

    public function execute()
    {
        header('content-type: text/html; charset=utf-8');
        return $this->{$this->command}();
    }

    public function import()
    {
        $importer = new SamepageImporter;
        $importer->setImportLocation(SamepageConfig::IMPORT_LOCATION);
        $importer->setDataStartRow($this->command_params['import_start_row']);

        $data_classname = ucfirst($this->data_type) . 'Data';

        $data_model = new $data_classname();
        $data_model->setImporter($importer);
        $data_model->setAPIClient($this->api_client);
        $data_model->import($this->command_params['import_filename']);
    }

    public function export()
    {
        if (!Data::isDataTypeAvailable($this->data_type) || 
            (!isset($this->command_params['export_start_at']) || !isset($this->command_params['export_end_at']))
        ) {
            // TODO; ADD LOG
            exit;
        }

        $exporter = new SamepageExporter();
        $exporter->setExportLocation(SamepageConfig::EXPORT_LOCATION);
        
        $data_classname = strtoupper($this->data_type) . 'Data';

        $data_model = new $data_classname();
        $data_model->setExporter($exporter);
        $data_model->setAPIClient($this->api_client);
        $data_model->setSearchFromDate($this->command_params['export_start_at']);
        $data_model->setSearchToDate($this->command_params['export_end_at']);

        return $data_model->export();
    }

    public function getFilters()
    {
        return array_filter($_GET, function($param, $key){
            return substr( $key, 0, 7 ) === 'filter_';
        }, ARRAY_FILTER_USE_BOTH );
    }
}
