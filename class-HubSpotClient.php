<?php

class HubSpotClient
{
    private $hubspot_api_endpoint;
    private $hubspot_api_key;

    /**
     * @param string $endpoint  A base URL of HubSpot API.
     */
    public function setAPIEndpoint($endpoint)
    {
        $this->hubspot_api_endpoint = $endpoint;
    }

    /**
     * @param string $endpoint  An API Key of HubSpot account.
     */
    public function setAPIKey($key)
    {
        if (empty($key)) {
            throw new Exception('APIキーが空です。APIキーは、config.phpファイルで設定してください。');
        }

        $this->hubspot_api_key = $key;
    }

    /**
     * Object ; Contact
     */
    public function contacts($params = [])
    {
        $properties = array(
            'hapikey' => $this->hubspot_api_key,
        );

        $properties = array_merge($properties, $params);
        return $this->post($this->hubspot_api_endpoint . '/crm/v3/objects/contacts?' . http_build_query($properties));
    }

    public function contactSearch($params = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/contacts/search');
        return $this->post($endpoint, $params);
    }

    public function contact($id, $params = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/contacts/' . $id, $params);
        return $this->get($endpoint);
    }

    public function contactAssociations($id, $association)
    {
        $endpoint = $this->getCRMEndpoint('objects/contacts/' . $id . '/associations/' . $association);
        return $this->get($endpoint);
    }

    public function contactChiikiBunrui($id, $param = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/2-2357464/' . $id, $param);
        return $this->get($endpoint);
    }

    public function contactUpdateBatch($params)
    {
        $endpoint = $this->getCRMEndpoint('objects/contacts/batch/update');
        return $this->post($endpoint, $params);
    }

    /**
     * Object ; Deal
     */
    public function deals()
    {
        $properties = array(
            'hapikey' => $this->hubspot_api_key,
            'limit' => 10,
            'properties' => implode(array('productcategory_c'), ','),
        );
        return $this->get($this->hubspot_api_endpoint . '/crm/v3/objects/deals?' . http_build_query($properties));
    }

    public function dealSearch($params = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/deals/search');
        return $this->post($endpoint, $params);
    }

    public function dealAssociations($id, $association)
    {
        $endpoint = $this->getCRMEndpoint('objects/deals/' . $id . '/associations/' . $association);
        return $this->get($endpoint);
    }

    public function dealUpdateBatch($params)
    {
        $endpoint = $this->getCRMEndpoint('objects/deals/batch/update');
        return $this->post($endpoint, $params);
    }

    /**
     * Object ; Line Items
     */
    public function lineItem($id, $params = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/line_items/' . $id, $params);
        return $this->get($endpoint);
    }

    public function productCreateBatch($params = [])
    {
        $endpoint = $this->getCRMEndpoint('objects/products/batch/create');
        return $this->post($endpoint, $params);
    }

    /**
     * Property list
     */
    public function properties($object_type)
    {
        $properties = array(
            'hapikey' => $this->hubspot_api_key,
        );
        return $this->get($this->hubspot_api_endpoint . '/crm/v3/properties/' . $object_type . '?' . http_build_query($properties));
    }

    public function property($object_type, $property_name)
    {
        $properties = array(
            'hapikey' => $this->hubspot_api_key,
        );
        return $this->get($this->hubspot_api_endpoint . '/crm/v3/properties/' . $object_type . '/' . $property_name . '?' . http_build_query($properties));
    }

    public function updateProperty($object_type, $property_name, $params)
    {
        $properties = array(
            'hapikey' => $this->hubspot_api_key,
        );
        $url = $this->hubspot_api_endpoint . '/crm/v3/properties/' . $object_type . '/' . $property_name . '?' . http_build_query($properties);

        return $this->patch($url, $params);
    }

    public function apiUsage()
    {
        $endpoint = $this->getEndpoint('/integrations/v1/limit/daily');
        return $this->get($endpoint);
    }

    /**
     * Make a GET HTTP request.
     */
    private function get($url)
    {
        $curl = curl_init();
 
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array("accept: application/json"),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }

    /**
     * Make a POST HTTP request.
     */
    private function post($url, $params)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => json_encode($params),
            CURLOPT_HTTPHEADER     => array(
                "accept: application/json",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }

    /**
     * Make a PATCH HTTP request.
     */
    private function patch($url, $params)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "PATCH",
            CURLOPT_POSTFIELDS     => json_encode($params),
            CURLOPT_HTTPHEADER     => array(
                "accept: application/json",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $body = json_decode($response, true);
            echo '<pre>';
            print_r($body);
        }
    }

    private function getCRMEndpoint($endpoint, $querystring = array())
    {
        $querystring = array_merge(
            array('hapikey' => $this->hubspot_api_key),
            $querystring
        );

        return $this->hubspot_api_endpoint . '/crm/v3/' . $endpoint . '?' . http_build_query($querystring);
    }

    private function getEndpoint($endpoint, $querystring = array())
    {
        $querystring = array_merge(
            array('hapikey' => $this->hubspot_api_key),
            $querystring
        );

        return $this->hubspot_api_endpoint . $endpoint . '?' . http_build_query($querystring);
    }
}
