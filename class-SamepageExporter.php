<?php

class SamepageExporter
{
    protected $export_location = '';

    public function setExportLocation($location)
    {
        $this->export_location = $location;
    }

    public function openFile($filename)
    {
        $current_time = new DateTime('now', new DateTimeZone('Asia/Tokyo'));
        $current_time = $current_time->format('YmdHis');
        
        $export_location = $this->export_location;
        
        $file = $export_location . '/' . $filename . '_' . $current_time . '.csv';
        return fopen($file, 'w');
    }

    public function putCSV($file, $data)
    {
        if (SamepageConfig::$dry_test) {
            return;
        }

        fputcsv($file, $data, ',', chr(0));
    }
}

