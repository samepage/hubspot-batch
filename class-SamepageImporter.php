<?php

class SamepageImporter
{
    protected $import_location = '';
    protected $import_filetype = 'csv';
    protected $import_files = array();
    protected $import_data_start_row = 0;

    protected $processing_queue_dir = __DIR__ . '/_queue';
    protected $processing_dir = __DIR__ . '/_processing';

    public function setImportLocation($location)
    {
        $this->import_location = $location;
    }

    public function setDataStartRow($row)
    {
        $this->import_data_start_row = $row;
    }

    public function getDataStartRow()
    {
        return $this->import_data_start_row > 0 ? $this->import_data_start_row - 1 : 0;
    }

    public function readCSV($filename)
    {
        $import_location = $this->import_location;
        $file = $import_location . '/' . $filename;

        $output = array();
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $output[] = $data;
            }
            fclose($handle);
        }

        return $output;
    }

    public function buildImportCreateParams($dataset)
    {
        $dataset = array_slice($dataset, $this->getDataStartRow());

        return [
            'inputs' => array_map(function($value) {
                return [
                    'properties' => [
                        "description"           => "Onboarding service for data product",
                        "hs_cost_of_goods_sold" => "600.00",
                        "name"                  => "Implementation Service ",
                        "price"                 => "6000.00"
                    ]
                ];
            }, $dataset, [])
        ];
    }

    public function buildImportUpdateParams($dataset, $structure, $definition, $id_key)
    {
        $dataset = array_slice($dataset, $this->getDataStartRow());

        return [
            'inputs' => array_map(function($data) use ($structure, $definition, $id_key) {

                return [
                    $id_key      => $data[$definition[$id_key]],
                    'properties' => array_map(function($value) use($definition, $data) {
                        if (preg_match('/^{.*}$/', $value)) {
                            $key = trim($value, '{}');
                            return $data[$definition[$key]];
                        }

                        return $value;
                    }, $structure)
                ];

            }, $dataset, [])
        ];
    }
}
