<?php

class SamepageLog
{
    protected $log_location = '';

    public static function log_error($exception, $report_data)
    {
        $log = new static();
        $current_time = new DateTime('now', new DateTimeZone('Asia/Tokyo'));
        $log_location = (defined(SamepageConfig::LOG_LOCATION) && !empty(SamepageConfig::LOG_LOCATION)) ? SamepageConfig::LOG_LOCATION : __DIR__ . '/logs';
        $log->setExportLocation($log_location);
        $file = $log->openFile('error');

        fwrite($file, '============' . PHP_EOL);
        fwrite($file, '=== エラー：' . $current_time->format('Y-m-d H:i:s') . '（Asia/Tokyo時間）' . PHP_EOL);
        fwrite($file, '=== エラーメッセージ：「' . $exception->getMessage() . '」' . PHP_EOL);
        fwrite($file, '=== エラー情報「' . PHP_EOL);
        fwrite($file, '=== data_type：「' . (isset($report_data['data_type']) ? $report_data['data_type'] : '') . '」' . PHP_EOL);
        fwrite($file, '=== command：「' . (isset($report_data['command']) ? $report_data['command'] : '') . '」' . PHP_EOL);
        fwrite($file, '=== command_params：「' . (isset($report_data['command_params']) ? $report_data['command_params'] : '') . '」' . PHP_EOL);
        fwrite($file, $exception->getTraceAsString() . PHP_EOL);
        fwrite($file, '=== 」' . PHP_EOL);
        fwrite($file, '===' . PHP_EOL);
        fclose($file);
    }

    public function setExportLocation($location)
    {
        $this->log_location = $location;
    }

    public function openFile($filename)
    {
        $current_time = new DateTime('now', new DateTimeZone('Asia/Tokyo'));
        $current_time = $current_time->format('Ymd');
        
        $log_location = $this->log_location;
        
        $file = $log_location . '/' . $filename . '_' . $current_time . '.txt';
        return fopen($file, 'a+');
    }
}
