<?php

class SamepageConfig
{
    // Developer debug mode.
    public static $debug    = true; // Print data on screen.
    public static $dry_test = false; // It will not export the file.

    public static $hubspot_api_endpoint = 'https://api.hubapi.com';

    // API key reference: https://knowledge.hubspot.com/integrations/how-do-i-get-my-hubspot-api-key
    public static $hubspot_api_key = '';

    const LOG_LOCATION = __DIR__ . '/logs';

    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    // インポート機能の設定。
    // --- START CONFIGURATION OF [Import] MODULE.
    const IMPORT_LOCATION = __DIR__ . '/imports';

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [Contact object]
    public static $data_contact_import_id_property_name = 'id';
    public static $data_contact_import_csv_structure = array(
        'id' => 0,
        'kokyaku_code_c' => 1,
    );

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [Deal Object]
    public static $data_deal_import_id_property_name = 'id';
    public static $data_deal_import_csv_structure = array(
        'id' => 0,
        'slip_numbre_c' => 1,
    );

    // --- END CONFIGURATION OF [Import] module.
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    // エクスポート機能の設定。
    // --- START CONFIGURATION OF [Export] MODULE.
    const EXPORT_LOCATION = __DIR__ . '/exports';

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [Contact object]
    public static $data_contact_fields = array(
        'hs_object_id',
        'lastname', 'lastname_kana_c',
        'firstname', 'first_name_kana_c',
        'phone', 'mobilephone',
        'zip', 'state', 'city', 'address',
        'email',
        'children_name_c', 'children_name_kana_c', 'children_phone_c',
        'birthday_c', 'age_c',
        'children_zip_c', 'children_address_c',
        'shipping_c',
        'introducer_c',
        'sodankai_year_c', 'sodankai_reservationcontents_c',
        'maedori_year_c', 'maedori_reservationcontents_c',
        'seijinsiki_year_c', 'seijinsiki_area_c', 'seijinsiki_reservationcontents_c',
        'sotugyosiki_year_c', 'sotugyosiki_schoolname_c', 'sotugyosiki_reservationcontents_c',
        'brother1_c', 'brother1_name_c', 'brother1_birthday_c',
        'brother2_c', 'brother2_name_c', 'brother2_birthday_c',
        'brother3_c', 'brother3_name_c', 'brother3_birthday_c',
        'flowline_c',
        'kakari_code_c',
        'regional_classification_c',
        'mapcode_c',
        'rank_c',
        'dm_c',
        'new_old_c',
        'inputperson_c',
        'preshoot_place1_c', 'preshoot_place2_c', 'preshoot_place3_c',
        'visitfrequency_c',
        'kimonofrequency_c',
        'preshoot_year_c',
        'adultyear_c',
        'graduationyear_c',
        'lastsalesdate_c',
        'lastdepositdate_c',
        'lastcomingdate_c',
        'lastvisitdate_c',
        'optimalvisit_c',
        'salesrank_c',
        'businessvisitdate_c',
        'salesbalance_c',
        'contac_to_sitisystem_day_c',
        'kokyaku_code_c',
        'description_c',
        'questionnaire1_c', 'questionnaire2_c', 'questionnaire3_c',
        'createdate',
        'lastmodifieddate',
        'tenpo',
    );
    public static $data_contact_export_sorts = array(
        array(
            'propertyName' => 'createdate', 
            'direction' => 'ASCENDING'
        )
    );

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [Deal Object]
    public static $data_deal_fields = array(
        'hs_object_id',
        'amount',
        'createdate',
        'deal_to_sitisystem_c',
        'hubspot_owner_id',
        'hs_lastmodifieddate',
        'slip_numbre_c',
        'salesperson1_c',
        'salesperson2_c',
        'salespersonratio1_c',
        'salespersonratio2_c',
        'rental_startday_c',
        'rental_endday_c',
        'deliveryday_c',
        'deposit_c',
        'balance_c',
        'paymentmethod_c',
        'paymentpattern_c',
        'paymentime_c',
        'sunpo01_c',
        'sunpo02_c',
        'sunpo03_c',
        'sunpo04_c',
        'sunpo05_c',
        'sunpo06_c',
        'sunpo07_c',
        'sunpo08_c',
        'sunpo09_c',
        'sunpo10_c',
        'sunpo11_c',
        'sunpo12_c',
        'sunpo13_c',
        'sunpo14_c',
        'sunpo15_c',
        'sunpo16_c',
        'sunpo17_c',
        'sunpo18_c',
        'sunpo19_c',
        'sunpo20_c',
        'sunpo21_c',
        'sunpo22_c',
        'sunpo23_c',
        'sunpo24_c',
        'sunpo25_c',
        'sunpo26_c',
        'sunpo27_c',
        'sunpo28_c',
        'sunpo29_c',
        'sale_rental_flg_c',
        'department_c',
        'deal_enpo_c',
    );
    public static $data_deal_export_limit = 100;
    public static $data_deal_export_sorts = array(
        array(
            'propertyName' => 'createdate', 
            'direction' => 'ASCENDING'
        )
    );

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [Line Item Object]
    public static $data_line_item_fields = array(
        'hs_object_id',
        'name',
        'hs_url',
        'product_flg_c',
        'description',
        'product_buy_c',
        'product_delivery_c',
        'product_purchasing_c',
        'description',
        'amount',
        'tax',
        'quantity',
    );

    // ////////////////////////////////////////////
    // --- CONFIGURATION OF [地域分類 Object]
    public static $data_chiiki_bunri_object = '2-2357464';
    public static $data_chiiki_bunri_fields = array( 'name' );

    // --- END CONFIGURATION OF [Export] module.
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
} 
