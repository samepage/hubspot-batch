<?php

class ContactData extends Data
{
    public function export()
    {
        $this->setPropertyField(SamepageConfig::$data_contact_fields);

        $result = $this->fetchAll('contact');
        $exported = $this->processExport($result['results']);
        return array_merge($result, $exported);
    }

    public function processExport($data)
    {
        $exported = array();

        $file = $this->exporter->openFile('contact_update');

        $contact_export_fields      = SamepageConfig::$data_contact_fields;
        $chiiki_bunri_export_fields = SamepageConfig::$data_chiiki_bunri_fields;

        // Set Header
        $this->exporter->putCSV($file, array_merge(
            $this->parseDataHeader($contact_export_fields, 'contact_'),
            $this->parseDataHeader($chiiki_bunri_export_fields, 'chiiki_bunri_')
        ));

        foreach ($data as $contact) {
            $contac_to_sitisystem_c = !isset($contact['properties']['contac_to_sitisystem_c']) || empty($contact['properties']['contac_to_sitisystem_c']) ? '1800-01-01' : $contact['properties']['contac_to_sitisystem_c'];
            $contac_to_sitisystem_c = new DateTime($contac_to_sitisystem_c, new DateTimeZone('Asia/Tokyo'));
            $updatedAt = new DateTime($contact['updatedAt'], new DateTimeZone('Asia/Tokyo'));

            if (empty($contact['properties']['kokyaku_code_c']) || ($contac_to_sitisystem_c->getTimestamp() >= $updatedAt->getTimestamp())) {
                continue;
            }

            $contact_chiiki_bunrui = $this->api_client->contactAssociations($contact['properties']['hs_object_id'], SamepageConfig::$data_chiiki_bunri_object);
            if (empty($contact_chiiki_bunrui['results'])) {
                $chiiki_bunrui_data = array();
            } else {
                $chiiki_bunrui_id = $contact_chiiki_bunrui['results'][0]['id'];
                $chiiki_bunrui = $this->api_client->contactChiikiBunrui($chiiki_bunrui_id, [ 'properties' => implode(',', $chiiki_bunri_export_fields) ]);

                $chiiki_bunrui_data = $this->getDataFromChiikiBunriProperties($chiiki_bunrui, $chiiki_bunri_export_fields);
            }

            $contact_data = array_merge(
                $this->getDataFromProperties($contact, $contact_export_fields),
                $chiiki_bunrui_data
            );

            $this->exporter->putCSV($file, $contact_data);
            $exported[] = $contact_data;
        }

        return array(
            'exported_total' => count($exported),
            'exported' => $exported
        );
    }

    public function import($filename)
    {
        $contacts = $this->importer->readCSV($filename);
        $this->processImport($contacts);        
    }

    public function processImport($dataset)
    {
        if (SamepageConfig::$debug) $this->printData($dataset);

        $start_row      = $this->importer->getDataStartRow();
        $id_key         = SamepageConfig::$data_contact_import_id_property_name;
        $csv_structure  = SamepageConfig::$data_contact_import_csv_structure;

        $update_data = $this->importer->buildImportUpdateParams($dataset, $this->importHubSpotPropertyStructure(), $csv_structure, $id_key);
        $result = $this->api_client->contactUpdateBatch($update_data);
        
        if (SamepageConfig::$debug) $this->printData($result);

        // TODO; Add log.
    }

    protected function importHubSpotPropertyStructure()
    {
        $current_time = $current_time = new DateTime('today midnight', new DateTimeZone('UTC'));

        return [
            'kokyaku_code_c' => '{kokyaku_code_c}',
            'contac_to_sitisystem_day_c' => $current_time->getTimestamp() * 1000,
        ];
    }
}
