<?php

class Data
{
    protected static $supported_data_type = [
        'contact',
        'deal',
        'line_item'
    ];

    protected $search_from_date  = '';
    protected $search_to_date    = '';
    protected $search_properties = array();
    protected $search_limit      = 100;
    protected $search_after      = 0;
    protected $data_filters      = [ [ 'filters' => [] ] ];

    protected $importer;
    protected $exporter;
    protected $api_client;

    public static function isDataTypeAvailable($type)
    {
        return in_array($type, self::$supported_data_type);
    }

    public function fetchAll($data)
    {
        try {
            $limit = 100;
            $iteration_round = 0;
            $total = 0;
            $results = array();

            $searched_result = array();
            do {
                $iteration_round++;
                $params = array(
                    'from'       => $this->getSearchDateTimestamp('from'), 
                    'to'         => $this->getSearchDateTimestamp('to'),
                    'properties' => $this->getPropertyField(),
                    'limit'      => $limit,
                    'after'      => ($iteration_round * $limit) - $limit
                );

                $results = $this->search($data, $params);

                $total         = isset($results['total']) && $results['total'] > 0 ? $results['total'] : 0;
                $searched_data = isset($results['results']) ? $results['results'] : array();

                $searched_result = array_merge($searched_result, $searched_data);

            } while ($total > 0 && count($searched_data) == $limit);

            return array(
                'total_iteration' => $iteration_round,
                'total' => count($searched_result),
                'results' => $searched_result
            );
        } catch (Exception $e) {
            //throw $th;
        }
    }

    public function search($data, $params)
    {
        $from_date  = $params['from'];
        $to_date    = $params['to'];
        $properties = isset($params['properties']) ? $params['properties'] : array();
        $sort       = isset($params['sort']) ? $params['sort'] : array(
            array(
                'propertyName' => 'createdate', 
                'direction' => 'ASCENDING'
            )
        );
        $limit      = isset($params['limit']) ? $params['limit'] : 100;
        $after      = isset($params['after']) ? $params['after'] : 0;

        $this->addDataFilters(['filter_createdate'=> $from_date]);

        $search_method = $data . 'Search';
        $result = $this->api_client->$search_method(array(
            'filterGroups' => $this->data_filters,
            'sorts'        => $sort,
            'properties'   => $properties,
            'limit'        => $limit,
            'after'        => $after
        ));

        if (!isset($result['results'])) {
            var_dump($result);
            $result['results'] = array();
            return $result;
        }

        $result['results'] = array_filter($result['results'], function($param, $key) use ($from_date, $to_date) {
            $item_createdate = new DateTime($param['createdAt']);
            $item_createdate = $item_createdate->getTimestamp() * 1000;

            return ($item_createdate >= $from_date && $item_createdate <= $to_date);
        }, ARRAY_FILTER_USE_BOTH);

        return $result;
    }

    public function setImporter($importer)
    {
        $this->importer = $importer;
    }

    public function setExporter($exporter)
    {
        $this->exporter = $exporter;
    }

    public function setAPIClient($api_client)
    {
        $this->api_client = $api_client;
    }

    public function setSearchFromDate($date)
    {
        $this->search_from_date = new DateTime($date, new DateTimeZone('Asia/Tokyo'));
    }

    public function setSearchToDate($date)
    {
        $this->search_to_date = new DateTime($date, new DateTimeZone('Asia/Tokyo'));
    }

    public function setPropertyField($fields)
    {
        $this->search_properties = $fields;
    }

    public function setSearchLimit($limit)
    {
        $this->search_limit = $limit;
    }

    public function setSearchAfter($after)
    {
        $this->search_after = $after;
    }

    public function getSearchDateTimestamp($type)
    {
        if (in_array($type, ['from', 'to'])) {
            $attribute_name = 'search_' . $type . '_date';
            return $this->$attribute_name->getTimestamp() * 1000;
        }
    }

    public function getPropertyField()
    {
        return $this->search_properties;
    }

    public function getSearchLimit()
    {
        return $this->search_limit;
    }

    public function getSearchAfter()
    {
        return $this->search_after;
    }

    public function addDataFilters($filters)
    {
        $this->data_filters[0]['filters'] = array_merge(
            $this->data_filters[0]['filters'],
            array_map(function($key, $value) {
                    switch ($key) {
                    case 'filter_createdate':
                        $operator = 'GTE';
                        break;
                    
                    default:
                        $operator = 'EQ';
                        break;
                }

                return [
                    'propertyName' => (strpos($key, 'filter_') === 0) ? substr($key, 7) : $key,
                    'value'        => $value,
                    'operator'     => $operator
                ];
            }, array_keys($filters), $filters)
        );
    }

    public function parseDataHeader($schema, $prefix = '')
    {   
        return array_map(function($item) use ($prefix){
            return $prefix . $item;
        }, $schema);
    }

    public function getDataFromProperties($data, $schema)
    {
        $csvdata = [];
        foreach($schema as $item) {
            if (!isset($data['properties'][$item])) {
                $csvdata[] = '""';
                continue;
            }

            $race_case_properties = ['shipping_c', 'kakari_code_c', 'flowline_c', 'eventcode_c','salesperson1_c','paymentmethod_c','paymentpattern_c','salesperson2_c','department_c','sale_rental_flg_c','chiiki_bunri_name'];
            if (in_array($item, $race_case_properties) && strpos($data['properties'][$item], "：")) {
                $data['properties'][$item] = substr($data['properties'][$item], 0, strpos($data['properties'][$item], "："));
            }

            $csvdata[] = '"' . $data['properties'][$item] . '"';
        }

        return $csvdata;
    }

    public function getDataFromChiikiBunriProperties($data, $schema)
    {
        $data['properties']['chiiki_bunri_name'] = $data['properties']['name'];

        $schema = array_map(function($value){
            if ($value == 'name') {
                return 'chiiki_bunri_name';
            }

            return $value;
        }, $schema, []);

        return $this->getDataFromProperties($data, $schema);
    }

    public function printData($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}
