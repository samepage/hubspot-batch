<?php

class DealData extends Data
{
    public function import($filename)
    {
        $deals = $this->importer->readCSV($filename);
        $this->processImport($deals);        
    }

    public function processImport($dataset)
    {
        if (SamepageConfig::$debug) $this->printData($dataset);

        $start_row      = $this->importer->getDataStartRow();
        $id_key         = SamepageConfig::$data_deal_import_id_property_name;
        $csv_structure  = SamepageConfig::$data_deal_import_csv_structure;
        
        $update_data = $this->importer->buildImportUpdateParams($dataset, $this->importHubSpotPropertyStructure(), $csv_structure, $id_key);
        $result = $this->api_client->dealUpdateBatch($update_data);

        if (SamepageConfig::$debug) $this->printData($result);

        // TODO; Add log.
    }

    public function importHubSpotPropertyStructure()
    {
        $current_time = $current_time = new DateTime('today midnight', new DateTimeZone('UTC'));

        return [
            'slip_numbre_c' => '{slip_numbre_c}',
            'deal_to_sitisystem_c' => $current_time->getTimestamp() * 1000,
        ];
    }

    public function export()
    {
        $this->setPropertyField(SamepageConfig::$data_deal_fields);

        $contacts = [];
        $deals = $this->fetchAll('deal');

        foreach($deals['results'] as $key => $deal) {
            $deal_id = $deal['id'];
            $deal_to_contact = $this->api_client->dealAssociations($deal_id, 'contacts');
            $deal_to_line_items = $this->api_client->dealAssociations($deal_id, 'line_items');

            $deal['line_items'] = [];
            foreach($deal_to_line_items['results'] as $line_items) {
                $deal['line_items'][] = $this->api_client->lineItem($line_items['id'], [
                    'properties' => implode(',', SamepageConfig::$data_line_item_fields)
                ]);
            }

            foreach($deal_to_contact['results'] as $contact) {
                $contact_properties = implode(',', SamepageConfig::$data_contact_fields);
                $contacts[$contact['id']]['info'] = $this->api_client->contact($contact['id'], [
                    'properties' => $contact_properties
                ]);
                $contacts[$contact['id']]['deals'][] = $deal;
            }
        }

        $exported =  $this->processExport($contacts);
        return array_merge($deals, $exported);
    }

    public function processExport($data)
    {
        $exported = array();

        $file_resources = [
            'file_contact_deal_new' => $this->exporter->openFile('contact_deal_new'),
            'file_deal_update'      => $this->exporter->openFile('deal_update'),
        ];

        $export_contact_fields      = SamepageConfig::$data_contact_fields;
        $export_chiiki_bunri_fields = SamepageConfig::$data_chiiki_bunri_fields;
        $export_deal_fields         = SamepageConfig::$data_deal_fields;
        $export_line_item_fields    = SamepageConfig::$data_line_item_fields;
        
        $csvdata_contact_deal_new = array_merge(
            $this->parseDataHeader($export_contact_fields, 'contact_'),
            $this->parseDataHeader($export_chiiki_bunri_fields, 'chiiki_bunri_'),
            $this->parseDataHeader($export_deal_fields, 'deal_'),
            $this->parseDataHeader($export_line_item_fields, 'line_item_')
        );

        $csvdata_deal_update = array_merge(
            $this->parseDataHeader($export_deal_fields, 'deal_'),
            $this->parseDataHeader($export_line_item_fields, 'line_item_')
        );

        $this->exporter->putCSV($file_resources['file_contact_deal_new'], $csvdata_contact_deal_new);
        $this->exporter->putCSV($file_resources['file_deal_update'], $csvdata_deal_update);

        foreach ($data as $contact) {
            $csvdata_contact = $this->getDataFromProperties($contact['info'], $export_contact_fields);

            $contact_chiiki_bunrui = $this->api_client->contactAssociations($contact['info']['properties']['hs_object_id'], SamepageConfig::$data_chiiki_bunri_object);
            if (empty($contact_chiiki_bunrui['results'])) {
                $csvdata_chiiki_bunri = [''];
            } else {
                $chiiki_bunrui_id = $contact_chiiki_bunrui['results'][0]['id'];
                $chiiki_bunrui = $this->api_client->contactChiikiBunrui($chiiki_bunrui_id, [ 'properties' => implode(',', $export_chiiki_bunri_fields) ]);
                $csvdata_chiiki_bunri = $this->getDataFromChiikiBunriProperties($chiiki_bunrui, $export_chiiki_bunri_fields);
            }

            foreach ($contact['deals'] as $deal) {
                $deal_to_sitisystem_c = empty($deal['properties']['deal_to_sitisystem_c']) ? '1800-01-01' : $deal['properties']['deal_to_sitisystem_c'];
                $deal_to_sitisystem_c = new DateTime($deal_to_sitisystem_c, new DateTimeZone('Asia/Tokyo'));
                $hs_lastmodifieddate = new DateTime($deal['properties']['hs_lastmodifieddate'], new DateTimeZone('Asia/Tokyo'));

                if (!empty($deal['properties']['slip_numbre_c']) && ($deal_to_sitisystem_c->getTimestamp() < $hs_lastmodifieddate->getTimestamp())) {
                    $selected_file_resource = 'file_deal_update';

                    $csvdata_deal = $this->getDataFromProperties($deal, $export_deal_fields);

                    if (empty($deal['line_items'])) {
                        $exported_data = array_merge($csvdata_deal, array_map(function(){ return ''; }, $export_line_item_fields));
                        $this->exporter->putCSV($file_resources[$selected_file_resource], $exported_data);
                        $exported[$deal['id']] = $exported_data;
                        continue;
                    }

                    foreach($deal['line_items'] as $item) {
                        $csvdata_item = $this->getDataFromProperties($item, $export_line_item_fields);

                        $exported_data = array_merge($csvdata_deal, $csvdata_item);
                        $this->exporter->putCSV($file_resources[$selected_file_resource], $exported_data);
                        $exported[$deal['id']] = $exported_data;
                    }
                } else {
                    $selected_file_resource = 'file_contact_deal_new';

                    $csvdata_deal = $this->getDataFromProperties($deal, $export_deal_fields);

                    if (empty($deal['line_items'])) {
                        $exported_data = array_merge($csvdata_contact, $csvdata_chiiki_bunri, $csvdata_deal, array_map(function(){ return ''; }, $export_line_item_fields));
                        $this->exporter->putCSV($file_resources[$selected_file_resource], $exported_data);
                        $exported[$deal['id']] = $exported_data;
                        continue;
                    }

                    foreach($deal['line_items'] as $item) {
                        $csvdata_item = $this->getDataFromProperties($item, $export_line_item_fields);

                        $exported_data = array_merge($csvdata_contact, $csvdata_chiiki_bunri, $csvdata_deal, $csvdata_item);
                        $this->exporter->putCSV($file_resources[$selected_file_resource], $exported_data);
                        $exported[$deal['id']] = $exported_data;
                    }
                }
            }
        }

        fclose($file_resources['file_contact_deal_new']);
        fclose($file_resources['file_deal_update']);

        return array(
            'exported_total' => count($exported),
            'exported' => $exported
        );
    }

}
