<?php

class ProductData extends Data
{
    public function import($filename)
    {
        $products = $this->importer->readCSV($filename);
        $this->processImport($products);        
    }

    public function processImport($dataset)
    {
        $dataset = [
            [ "Test 1", "89", "1000.00" ],
            [ "Test 2", "99", "2000.00" ],
        ];
        
        if (SamepageConfig::$debug) $this->printData($dataset);

        $create_data = $this->importer->buildImportCreateParams($dataset);
        $result = $this->api_client->productCreateBatch($create_data);

        if (SamepageConfig::$debug) $this->printData($result);

        // TODO; Add log.
    }
}
