<?php
set_time_limit(0);

define('BASE_URL', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
define('BASE_DIR', __DIR__);

require __DIR__ . '/app.php';
require __DIR__ . '/config.php';
require __DIR__ . '/class-HubSpotClient.php';
require __DIR__ . '/class-SamepageImporter.php';
require __DIR__ . '/class-SamepageExporter.php';
require __DIR__ . '/class-SamepageLog.php';
require __DIR__ . '/data/class-Data.php';
require __DIR__ . '/data/class-ContactData.php';
require __DIR__ . '/data/class-DealData.php';
require __DIR__ . '/data/class-ProductData.php';

$data_type       = isset($_GET['data_type']) ? $_GET['data_type'] : '';
$command         = isset($_GET['command']) ? $_GET['command'] : '';
$command_params  = [
    'export_start_at'  => isset($_GET['export_start_at']) ? $_GET['export_start_at'] : '',
    'export_end_at'    => isset($_GET['export_end_at']) ? $_GET['export_end_at'] : '',
    'import_filename'  => isset($_GET['import_filename']) ? $_GET['import_filename'] : '',
    'import_start_row' => isset($_GET['import_start_row']) ? $_GET['import_start_row'] : '',
];

// 「COMMAND」または「DATA_TYPE」が設定されていない場合、インターフェースを表示します。
// Display an interface if the 「COMMAND」or「DATA_TYPE」 is not set.
if (empty($command) || empty($data_type)) {
    require __DIR__ . '/interface.php';
    exit;
}


try {
    // HubSpot APIクライアントを起動する（Initiate HubSpot API client）。
    $hubspot = new HubSpotClient;
    $hubspot->setAPIKey(SamepageConfig::$hubspot_api_key);
    $hubspot->setAPIEndpoint(SamepageConfig::$hubspot_api_endpoint);

    // アプリケーションを開始する（Initiate application）。
    $app = App::getInstance()->init($hubspot, $command, $data_type, $command_params);
    $result = $app->execute();
} catch (Exception $e) {
    if (SamepageConfig::$debug) {
        echo 'プログラムが正しく処理できません。<br/>';
        echo 'エラーメッセージ「' . $e->getMessage() . '」';
    }

    $report_data = [
        'data_type'      => $data_type,
        'command'        => $command,
        'command_params' => implode(',', [
            'export_start_at：' . $command_params['export_start_at'],
            'export_end_at：' . $command_params['export_end_at'],
            'import_filename：' . $command_params['import_filename'],
            'import_start_row：' . $command_params['import_start_row'],
        ])
    ];
    SamepageLog::log_error($e, $report_data);
}

require __DIR__ . '/interface.php';
