<html>
    <head>
        <style>
            html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;

                font-family: Helvetica, "Trebuchet MS", Verdana, sans-serif;
                line-height: 1.4em;
            }
            h1 {
                font-size: 2em;
                font-weight: 700;
                margin-bottom: 1em;
            }

            h2 {
                font-size: 1.4em;
                font-weight: 700;
                margin-bottom: 1em;
            }

            h3 {
                font-size: 1.2em;
                font-weight: 700;
                margin-bottom: 1em;
            }

            hr {
                margin: 4em 0;
            }

            strong {
                font-weight: 700;
            }

            em {
                font-style: italic;
                font-size: 75%;
            }
            
            small {
                font-size: 70%;
            }

            dt {
                font-weight: 700;
            }

            dd {
                margin-bottom: 1em;
            }

            a.button {
                text-decoration: none;
                color: #000;
                display: inline-block;
            }

            .button {
                border: 1px solid #3f84ca;
                border-radius: 3px;
                background-color: #6fa4da;
                padding: 10px;
            }

            .container {
                padding: 2em;
                clear: both;
            }
            .note {
                padding: 1em;
                background: #6fa4da;
                color: #fff;
            }

            .left-content {
                float: left;
                width: 22%;
            }

            .right-content {
                float: left;
                width: 78%;
            }

            .row {
                padding: 15px;
            }

            .card {
                padding: 1em;
                box-shadow: 1px 1px 4px 3px #ddd;
            }

            .card.note {
                box-shadow: 0px 0px 10px -1px #245990;
                color: #fff;
            }

        </style>
    </head>
<body>
    <div class="container">
        <div class="left-content">
            <div class="row">
                <div class="card note">
                    <?php
                    $hubspot = new HubSpotClient;
                    $hubspot->setAPIKey(SamepageConfig::$hubspot_api_key);
                    $hubspot->setAPIEndpoint(SamepageConfig::$hubspot_api_endpoint);
                    $api_info = $hubspot->apiUsage();
                    ?>

                    <h3>API 日数制限</h3>
                    <dl>
                        <dt>1日あたりの使用制限</dt><dd><?php echo number_format($api_info[0]['usageLimit']); ?><small>リクエスト</small></dd>
                        <dt>現在の使用状況</dt><dd><?php echo number_format($api_info[0]['currentUsage']); ?><small>リクエスト</small></dd>
                        <dt>API Usage limit reset at </dt><dd><?php echo date('Y-m-d H:i:s', ($api_info[0]['resetsAt']/1000)); ?></dd>
                    </dl>
                </div>
            </div>

            <?php if (isset($result)) : ?>
                <div class="row">
                    <div class="card">
                        <h2>Result</h2>
                        <dl>
                            <?php if (isset($_REQUEST['data_type'])) : ?>
                                <dt>Source Data</dt><dd><?php echo $_REQUEST['data_type']; ?></dd>
                            <?php endif; ?>

                            <?php if (isset($_REQUEST['command'])) : ?>
                                <dt>Command</dt><dd><?php echo $_REQUEST['command']; ?></dd>
                            <?php endif; ?>

                            <?php if (isset($_REQUEST['export_start_at'])) : ?>
                                <dt>時間からデータを検索する。</dt><dd><?php echo $_REQUEST['export_start_at']; ?></dd>
                            <?php endif; ?>

                            <?php if (isset($_REQUEST['export_end_at'])) : ?>
                                <dt>今回までのデータを検索する。</dt><dd><?php echo $_REQUEST['export_end_at']; ?></dd>
                            <?php endif; ?>

                            <?php if (isset($result['total'])) : ?>
                                <dt>Total Data Found</dt><dd><?php echo number_format($result['total']); ?></dd>
                            <?php endif; ?>

                            <?php if (isset($result['exported_total'])) : ?>
                                <dt>Total Data Exported</dt><dd><?php echo number_format($result['exported_total']); ?></dd>
                            <?php endif; ?>
                        </dl>
                        <pre>
                            <?php print_r($result); ?>
                        </pre>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="right-content">
            <div class="row">
                <h1>Exporter</h1>
                <h2>使いたい機能は何ですか？<br/><small>（What feature do you wish to use?）</small></h2>
                <form method="GET">
                    <input type="hidden" name="command" value="export">

                    <h3>設定 (config.php)<h3>
                    <div style="background: #dedede; margin-bottom: 1em; padding: 1em; font-size: 80%;">
                        DEBUG: <strong><?php echo SamepageConfig::$debug ? 'ON' : 'OFF'; ?></strong><br/>
                        DRY_TEST: <strong><?php echo SamepageConfig::$dry_test ? 'ON' : 'OFF'; ?></strong><br/>
                        EXPORT_LOCATION: <strong><?php echo SamepageConfig::EXPORT_LOCATION; ?></strong>
                    </div>

                    <div style="margin-bottom: 1em;">
                        <p class="note"><em>
                            * 注）時間はアジア/東京タイムゾーンで入力してください。<br/>
                            HubSpotは「UTC」タイムゾーンを使用しています。このプログラムは、「Asia/Tokyo」のタイムゾーンを自動的にUTCに戻します。</em></p><br/>
                        Export from: <input name="export_start_at" placeholder="ex: 2021-06-13 18:30:00"> (時間からデータを検索する。)
                        to: <input name="export_end_at" placeholder="ex: 2021-06-14 17:29:59"> (今回までのデータを検索する。)
                    </div>

                    <button type="submit" name="data_type" value="contact">コンタクトをエクスポートする（Export Contact）。</button>
                    <button type="submit" name="data_type" value="deal">取引をエクスポートする（Export Deal）。</button>
                </form>

                <hr>

                <h1>Importer</h1>
                <h2>インポート機能を手動実行する。<br/><small>（execute the import function manually）</small></h2>
                <form method="GET">
                    <input type="hidden" name="command" value="import">

                    <h3>設定 (config.php)<h3>
                    <div style="background: #dedede; margin-bottom: 1em; padding: 1em; font-size: 80%;">
                        DEBUG: <strong><?php echo SamepageConfig::$debug ? 'ON' : 'OFF'; ?></strong><br/>
                        DRY_TEST: <strong><?php echo SamepageConfig::$dry_test ? 'ON' : 'OFF'; ?></strong><br/>
                        IMPORT_LOCATION: <strong><?php echo SamepageConfig::IMPORT_LOCATION; ?></strong>
                    </div>

                    <div style="margin-bottom: 1em;">
                        Filename: <input name="import_filename" placeholder="ex: contact.csv"> (ファイル名。)
                        Start row: <input name="import_start_row" placeholder="ex: 2"> (データのスタートラインです。)
                    </div>

                    <button type="submit" name="data_type" value="contact">コンタクトをインポートする（Import Contact）。</button>
                    <button type="submit" name="data_type" value="deal">取引をインポートする（Import Deal）。</button>
                    <button type="submit" name="data_type" value="product">製品をインポートする（Import Line Item）。</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>